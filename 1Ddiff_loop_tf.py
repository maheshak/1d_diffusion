import tensorflow as tf
import time
import os



def diff_loop_tf(L,T,Nx,Nt,a,I):
    x = tf.linspace(0.0,L,Nx+1)
    dx = x[1] - x[0]
    dt = T/Nt
    F = a*(dt/(dx**2))
    ## initial condition
    u = I(x)
    i = tf.constant(0)
    c = lambda i,u: i < Nt
    b = lambda i,u: (i+1, tf.concat([tf.constant([0.0]),u[1:Nx] + F*(u[0:Nx-1] - 2*u[1:Nx] + u[2:Nx+1]),tf.constant([0.0])],0))
    i, u = tf.while_loop(c,b,[i,u])
    return u
    

def main():
    start = time.time()
    points = POINTS
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    u = diff_loop_tf(1000000.0,1.0,points,10000,0.8,tf.cos)
    with tf.Session() as sess:
        result = sess.run(u)
    done = time.time()
    elapsed = done - start
    print(points,elapsed)

if __name__=='__main__':
    main()
