import numpy as np
import time

def diff_loop(L,T,Nx,Nt,a,I):
    x = np.linspace(0,L,Nx+1)
    dx = x[1] - x[0]
    dt = T/Nt
    F = a*(dt/(dx**2))
    ## initial condition
    u = I(x)

    ## time step
    for n in range(0,Nt):
        u[1:Nx] = u[1:Nx] + F*(u[0:Nx-1] - 2*u[1:Nx] + u[2:Nx+1])

        ## Boundary Conditions
        u[0] = 0
        u[Nx] = 0

    
    return u

def main():
    points = POINTS
    start = time.time()
    result = diff_loop(1000000.0,1.0,points,10000,0.8,np.cos)

    done = time.time()
    elapsed = done - start
    print(points,elapsed)

if __name__=='__main__':
    main()



 
