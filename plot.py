import numpy as np
import matplotlib.pyplot as plt

def main():
    res_np = np.loadtxt(open('result_np.csv','rb'),delimiter=' ',skiprows=1)
    res_tf_cpu = np.loadtxt(open('result_tf_cpu.csv','rb'),delimiter=' ',skiprows=1)
    res_cc = np.loadtxt(open('result_cc.csv','rb'),delimiter=' ',skiprows=1)
    res_tf = np.loadtxt(open('result_tf.csv','rb'),delimiter=' ',skiprows=1)
    res_cu = np.loadtxt(open('result_cu.csv','rb'),delimiter=' ',skiprows=1)
    res_torch = np.loadtxt(open('result_torch.csv','rb'),delimiter=' ',skiprows=1)
    res_pycu = np.loadtxt(open('result_pycuda.csv','rb'),delimiter=' ',skiprows=1)

    plt.loglog(res_np[:,0],res_np[:,1])
    plt.loglog(res_cc[:,0],res_cc[:,1])
    plt.loglog(res_tf_cpu[:,0],res_tf_cpu[:,1])
    plt.loglog(res_tf[:,0],res_tf[:,1])
    plt.loglog(res_torch[:,0],res_torch[:,1])
    plt.loglog(res_cu[:,0],res_cu[:,1])
    plt.loglog(res_pycu[:,0],res_pycu[:,1])
  
    plt.legend(('numpy','mpi-blitz++','tensorflow-cpu','tensorflow-gpu-py','pytorch-cuda','cuda-c','pyCUDA'))
    plt.title('1D diffusion - finite difference benchmarks (10000 time steps)')
    plt.xlabel('Number of points')
    plt.ylabel('Time taken(s)')
    plt.show()



if __name__=='__main__':
    main()