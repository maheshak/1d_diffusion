import time
import torch
import sys

def diff_loop(L,T,Nx,Nt,a,I):
    cuda = torch.device('cuda')
    x = torch.linspace(0.0,L,Nx+1,dtype=torch.float32,device=cuda)
    dx = x[1] - x[0]
    dt = T/Nt
    F = a*(dt/(dx**2))
    ## initial condition
    u = I(x)
    z = torch.tensor([0],dtype=torch.float32,device=cuda)

    ## time step
    for n in range(0,Nt):
        u = torch.cat([z,u[1:Nx] + F*(u[0:Nx-1] - 2*u[1:Nx] + u[2:Nx+1]),z],0)
    
    return u.to(torch.device("cpu"))

def main():
    sys.excepthook = sys.__excepthook__
    points = POINTS
    start = time.time()
    result = diff_loop(1000000.0,1.0,points,10000,0.8,torch.cos)

    done = time.time()
    elapsed = done - start
    print(points,elapsed)

if __name__=='__main__':
    main()


