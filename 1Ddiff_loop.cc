// #include <mpi.h>
#include <cmath>
#include <ctime>
#include <blitz/array.h>
#include <iostream>
#include <mpi.h>

using namespace blitz;
using namespace std;

const float interval_begin = 0.0;
const float interval_end = 1000000.0;

const float temperature_begin = 0.0;
const float temperature_end = 0.0;

const int tag_init = 1;
const int tag_computing = 2;
const int tag_gathering = 3;

float initial_temperature(float x) {
    return cos(x);
}

float compute_next_step(float F,float y1, float y2, float y3) {
    return y2 + F* (y1 - 2 * y2 + y3);
}


int main(int argc, char *argv[]) {
    clock_t start, end;
    start = clock();

    MPI_Init(&argc, &argv);

    int points = POINTS;
    float time_to_integrate = 1.0;
    int iterations_num = 10000;
    float a = 0.8;
    float time_step = time_to_integrate/iterations_num;
    float x_step = (interval_end - interval_begin) / (points - 1);
    float F = a*time_step/pow(x_step,2);

    int processes = 1;
    int my_rank = 0;

    MPI_Comm_size(MPI_COMM_WORLD, &processes);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    // 1. Master process fills the buffer with initial data
    Array<float,1> ys;
    if (my_rank == 0) {
        ys.resize(points);
        for (int i = 0; i < points; i++) {
            ys(i) = initial_temperature(interval_begin + i * (interval_end - interval_begin) / points);
        }
        ys(0) = temperature_begin;
        ys(points - 1) = temperature_end;

    } 

    // 2. Processes determine which the subintervals they are responsible for
    Array<int,1> points_begin(processes);
    Array<int,1> points_end(processes);
    Array<int,1> points_num(processes);
    
    int basic_points_num = points / processes;
    int points_left = points % processes;

    for (int rank = 0; rank < processes; rank++) {
        int cur_points_begin, cur_points_end;
        if (rank < points_left) {
            cur_points_begin = rank * basic_points_num;
            cur_points_end = cur_points_begin + basic_points_num - 1;
        } else {
            cur_points_begin = (basic_points_num + 1) * points_left 
                + (rank - points_left) * basic_points_num;
            cur_points_end = cur_points_begin + basic_points_num - 1;
        }

        int cur_points_num = basic_points_num;

        if (rank > 0) {
            cur_points_begin -= 1;
            cur_points_num += 1;
        }
        if (rank < processes - 1) {
            cur_points_end += 1;
            cur_points_num += 1;
        }

        points_begin(rank) = cur_points_begin;
        points_end(rank) = cur_points_end;
        points_num(rank) = cur_points_num;
    }

    int my_points_num = points_num(my_rank);
    int my_points_begin = points_begin(my_rank);
    int my_points_end = points_end(my_rank);

    // 3. Master process distributes the initial values among the processes
    Array<float,1> buf1(my_points_num);
    Array<float,1> buf2(my_points_num);
    Array<float,1> my_ys = buf1;
    Array<float,1> my_ys_temp = buf2;
    
    if (my_rank == 0) {
        for (int rank = 1; rank < processes; rank++) {
             MPI_Send(ys.dataZero() + points_begin(rank), points_num(rank), MPI_FLOAT, 
                     rank, tag_init, MPI_COMM_WORLD);  
        }
        
        for (int i = my_points_begin; i < my_points_end; i++) 
            my_ys(i) = ys(i);

    } else {
        MPI_Recv(my_ys.dataZero(), my_points_num, MPI_FLOAT, 0, tag_init, 
                MPI_COMM_WORLD,   MPI_STATUS_IGNORE);
    }

    // 4. Processes start computing the solution
    for (int iteration = 0; iteration < iterations_num; iteration++) {
       
        int first_index_to_compute = 1;
        int last_index_to_compute = my_points_num - 2;

        for (int i = first_index_to_compute; i <= last_index_to_compute; i++) {
            my_ys_temp(i) = compute_next_step(F, my_ys(i - 1), my_ys(i), my_ys(i + 1));
        }

        // Pass the border points to neighbor processes
        if (my_rank % 2) {
            if (my_rank < processes - 1) {
    //            fprintf(stderr, "\tProcess %d -> Process %d\n", my_rank, my_rank + 1);
                MPI_Send(my_ys_temp.dataZero()+last_index_to_compute, 1, MPI_FLOAT, my_rank + 1,
                         tag_computing, MPI_COMM_WORLD);
            }
            if (my_rank > 0) {
      //          fprintf(stderr, "\tProcess %d -> Process %d\n", my_rank, my_rank - 1);
                MPI_Send(my_ys_temp.dataZero()+first_index_to_compute, 1, MPI_FLOAT, my_rank - 1,
                         tag_computing, MPI_COMM_WORLD);
            }
            if (my_rank < processes - 1) {
                MPI_Recv(my_ys_temp.dataZero()+(my_points_num - 1), 1, MPI_FLOAT, my_rank + 1, 
                         tag_computing, MPI_COMM_WORLD,   MPI_STATUS_IGNORE);
            } 
            if (my_rank > 0) {
                MPI_Recv(my_ys_temp.dataZero() + 0, 1, MPI_FLOAT, my_rank - 1,
                         tag_computing, MPI_COMM_WORLD,   MPI_STATUS_IGNORE);
            }
        } else {
            if (my_rank > 0) {
                MPI_Recv(my_ys_temp.dataZero()+0, 1, MPI_FLOAT, my_rank - 1,
                         tag_computing, MPI_COMM_WORLD,   MPI_STATUS_IGNORE);
            }
            if (my_rank < processes - 1) {
                MPI_Recv(my_ys_temp.dataZero()+(my_points_num - 1), 1, MPI_FLOAT, my_rank + 1, 
                         tag_computing, MPI_COMM_WORLD,   MPI_STATUS_IGNORE);
            }
            if (my_rank > 0) {
        //        fprintf(stderr, "\tProcess %d -> Process %d\n", my_rank, my_rank - 1);
                MPI_Send(my_ys_temp.dataZero()+first_index_to_compute, 1, MPI_FLOAT, my_rank - 1,
                         tag_computing, MPI_COMM_WORLD);
            }
            if (my_rank < processes - 1) {
          //      fprintf(stderr, "\tProcess %d -> Process %d\n", my_rank, my_rank + 1);
                MPI_Send(my_ys_temp.dataZero()+last_index_to_compute, 1, MPI_FLOAT, my_rank + 1,
                         tag_computing, MPI_COMM_WORLD);
            }            
        }

        // Swap the pointers
        Array<float,1> t = my_ys;
        my_ys = my_ys_temp;
        my_ys_temp = t;
    }

    // 5. Collect the partial results 
    if (my_rank == 0) {
        for (int i = 0; i <= my_points_end - 1; i++) {
            ys(i) = my_ys(i);
        }

        for (int rank = 1; rank < processes - 1; rank++) {
            MPI_Recv(ys.dataZero() + points_begin(rank) + 1, points_num(rank) - 2,
                     MPI_FLOAT, rank, tag_gathering, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        if (processes > 1){
            MPI_Recv(ys.dataZero() + points_begin(processes - 1) + 1, points_num(processes - 1) - 1,
                     MPI_FLOAT, processes - 1, tag_gathering, MPI_COMM_WORLD,   MPI_STATUS_IGNORE);
        }
    } else {
        int count = my_points_num - 2;
        if (my_rank == processes - 1)
            count = my_points_num - 1;
        MPI_Send(my_ys.dataZero()+1, count, MPI_FLOAT, 0, tag_gathering, MPI_COMM_WORLD);
    }

    end = clock();
    double cpu_time_used =  ((double) (end - start)) / CLOCKS_PER_SEC;

    // 6. Print the result out
    if (my_rank == 0) {
        cout << setprecision(8) << fixed;
        cout << points << " "<< cpu_time_used << endl;
    }



    MPI_Finalize();

    return EXIT_SUCCESS;
}