#include<stdio.h>
#include<math.h>
#include<time.h>

#define dt 0.0001 // discrete-time resolution
#define alpha 0.8 // thermal diffusivity
#define r alpha*dt/(dx*dx)
#define L 1000000.0
#define kend 10000 // # of time step
#define N POINTS
#define dx L/POINTS




__global__ void kernel(float* u){

    int i = blockIdx.x*blockDim.x + threadIdx.x;
  
	if( i > 0 && i < N-1){
	   u[i] = r*(u[i+1]-2*u[i]+u[i-1]) + u[i];
	}
}


int main(){
    clock_t start,end;
    start = clock();
    float *u, *uDev;
    int size1 = N*sizeof(float);

    cudaMalloc((void**) &uDev,size1);
    u = (float*)malloc(size1);

   // initial condition
    for(int i=0;i<N;i++)
    {
	   u[i] = cos(i*dx); // cosine func.
    }

    cudaMemcpy(uDev,u,size1,cudaMemcpyHostToDevice);
    int dimThreads = 1024;
    int dimBlock = N/dimThreads;
    for(int k=0;k<kend;k++) // time-loop
    {
        kernel<<<dimBlock,dimThreads>>>(uDev);
    }
 
   free(u);
   cudaFree(uDev);
   end = clock();
   double cpu_time_used =  ((double) (end - start)) / CLOCKS_PER_SEC;
   printf("%d %f\n",N,cpu_time_used);
   return 0;
}