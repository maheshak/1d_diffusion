#!/bin/bash

i="10"
limit="1000000000"

#echo "#Points    time  (for numpy at 10000 time steps)" > result_np.csv
#while [ $i -lt $[$limit/10] ]
#do

#m4 -DPOINTS="$i" 1Ddiff_loop.py | python3
#i=$[$i*10]

#done >> result_np.csv

i="10"

echo "#Points    time  (for tensorflow at 10000 time steps)" > result_tf_cpu.csv
while [ $i -lt $limit ]
do

m4 -DPOINTS="$i" 1Ddiff_loop_tf.py | python3
i=$[$i*10]

done >> result_tf_cpu.csv

#i="10"

#echo "#Points    time  (for mpi-blitz++ at 10000 time steps)" > result_cc.csv
#while [ $i -lt $limit ]
#do

#m4 -DPOINTS="$i" 1Ddiff_loop.cc > temp.cc
#mpicxx temp.cc
#mpirun -np 32 ./a.out
#i=$[$i*10]

#done >> result_cc.csv

#i="10"

#echo "#Points    time  (for cuda-c at 10000 time steps)" > result_cu.csv
#while [ $i -lt $limit ]
#do

#nvcc -DPOINTS="$i" 1Ddiff_loop.cu
#./a.out
#i=$[$i*10]

#done >> result_cu.csv

#rm temp.*
#i="10"

#echo "#Points    time  (for pytorch at 10000 time steps)" > result_torch.csv
#while [ $i -lt $limit ]
#do

#m4 -DPOINTS="$i" 1Ddiff_loop_torch.py | python3
#i=$[$i*10]

#done >> result_torch.csv

#i="10"

#echo "#Points    time  (for pyCUDA at 10000 time steps)" > result_pycuda.csv
#while [ $i -lt $limit ]
#do

#m4 -DPOINTS="$i" 1Ddiff_loop_cuda.py | python
#i=$[$i*10]

#done >> result_pycuda.csv