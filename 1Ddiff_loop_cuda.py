import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule
import numpy as np
import time

def diff_loop(L,T,Nx,Nt,a,I):
    mod = SourceModule("""
    __global__ void kernel(float* u,float r,int N){

        int i = blockIdx.x*blockDim.x + threadIdx.x;
    
        if( i > 0 && i < N-1){
            u[i] = r*(u[i+1]-2*u[i]+u[i-1]) + u[i];
        }
    }
    """)
    update = mod.get_function("kernel")
    x = np.linspace(0,L,Nx+1)
    dx = x[1] - x[0]
    dt = T/Nt
    F = a*(dt/(dx**2))
    ## initial condition
    u = I(x)
    u = u.astype(np.float32)
    u_gpu = cuda.mem_alloc(u.nbytes)
    cuda.memcpy_htod(u_gpu, u)
    N = np.int32(Nx)
    r = np.float32(F)
    ## time step
    for n in range(0,Nt):
        update(u_gpu,r,N,block=(1024,1,1),grid=(Nx//1024,1))
    
    cuda.memcpy_dtoh(u, u_gpu)

    
    return u

def main():
    points = POINTS
    start = time.time()
    result = diff_loop(1000000.0,1.0,points,10000,0.8,np.cos)

    done = time.time()
    elapsed = done - start
    print points,elapsed

if __name__=='__main__':
    main()